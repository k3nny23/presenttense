process.env.NODE_ENV = 'test';

var app = require('../../app')
  , supertest = require('supertest-session')
  , cheerio = require('cheerio')
  // , connect = require('../../libs/db_connect')()
  // , mongoose = require('mongoose')
  , dropCollections = require('../../libs/drop_collections');
;

  var User = app.models.user;

  function extractCsrfToken(res){
    var $ = cheerio.load(res.text);
    return $('[name=_csrf]').val();
  }

  describe('In controller auth', function(){

    var login = {user: {username: "testAAA", password: "test" }}
      , csrfToken
      , authenticatedSession
    ;

    before((done) => {
      // console.log(User);
      // dropCollections(User);
      User.collection.drop();
      done();
    });

    beforeEach(function(done){
      session = supertest(app);
      session.get('/').expect(200)
      .end(function(err, res){
        if(err) return done(res);
        csrfToken = extractCsrfToken(res);
        done();
      });
    });

    it('shoud return status 200 in GET /',
      function(done){
        session.get('/')
               .end(function(err, res){
                 if(err) return done(err);
                res.status.should.eql(200);
                done();
              });// end: get > end
    }); // end: it

    it('should redirec to /manager when POST /register is successed',
      function(done){
        login._csrf = csrfToken;

        session.post('/register')
               .send(login)
               .end(function(err, res){
                if(err) return done(err);
                res.headers.location.should.eql("/manager");
                done();
              }); // end: post > send > end
    }); // end: it

    it('shoud redirect to GET /manager when use POST /login',
      function(done){
        login._csrf = csrfToken;
        session.post('/login')
                .send(login).end(function(err, res){
                  if(err) return done(err);
                  res.headers.location.should.eql('/manager');
                  done();
                }); // end: send > end
    });// end: it
  }); // end: describe
