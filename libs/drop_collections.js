var async = require('async')
  , _ = require('underscore')
;

var Helpers = function(mongoose){
  this.mongoose = mongoose || require('mongoose')

  this.dropCollections =  function(callback) {
    var collections = _.keys(mongoose.connection.collections)
    async.forEach(collections, function(collectionName, done){
      var collection = mongoose.connection.collection[collectionName]
      console.log(collection);
      collection.drop(function(err){
        if(err && err.message != 'ns not found') done(err)
        done(null)
      })
    }, callback)
  }   // end: dropCollections
} // end:  helpers

module.exports = Helpers;
