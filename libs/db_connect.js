var mongoose = require('mongoose')
  , bluebird = require('bluebird')
  , config = require('../config.json')
  , env = process.env.NODE_ENV || "development"
  , url = config.MONGODB[env]
  , single_connection
;

module.exports = function(){
  // doesn't it connect? then connect
  if(!single_connection){
    mongoose.Promise = bluebird;
    single_connection = mongoose.connect(url);
  }
  return single_connection;
}
