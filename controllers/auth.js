module.exports = function(app){

var User = app.models.user;

var AuthController = {
  index: function(req, res){
    res.render("auth/index");
  },
  login: function(req, res){
    var query = {username: req.body.user.username};
    User.findOne(query)
        .select('username password')
        .exec(function(error, user){
          if(!user){
            // TO TEST
            // res.body.warning = {
            //   type: "auth",
            //   message: "Login has failed, your email or password is wrong!"
            // };
            res.redirect('/');
          }else{
              req.session.user = user;
              res.redirect('/manager');
          } // end: else
        }); // end: exec
    }, // end: login
    register: function(req, res){
      var user = req.body.user;
      User.create(user, function(err, user){
        if(err){
          console.log(err);
          // send user to start
          res.redirect('/');
        }else{
          // put user in session and redirect
          req.session.user = user;
          res.redirect('/manager');
        } // end: else
      }); // end: create
    }, // end: register
    logout: function(req, res){
      res.redirect('/');
    } // end: logout
  } // end: controller
  return AuthController;
};
