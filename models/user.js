module.exports = function(app){
  var db = require('../libs/db_connect')()
    , Schema = require('mongoose').Schema;

    // sub collection
    var profile = Schema({
        name: String
      , picture: String
      , birthday: String
    });

    // collection
    var user = Schema({
        username: {type: String, require: true, index: {unique: true}}
      , email: {type: String, require: true}
      , password: {type: String, require: true}
      , profile: profile
    });

    return db.model('user', user);
}
