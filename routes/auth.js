module.exports = function(app){

  var auth = app.controllers.auth;

  app.get('/', auth.index);
  app.post('/login', auth.login);
  app.post('/register', auth.register);
};
