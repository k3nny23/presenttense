var express = require('express')
  , cfg = require('./config.json')
  , path = require('path')
  , load = require('express-load')
  , bodyParser = require('body-parser')
  , cookieParser = require('cookie-parser')
  , expressSession = require('express-session')
  , methodOverride = require('method-override')
  , compression = require('compression')
  , csurf = require('csurf')
  , redisAdapter = require('socket.io-redis')
  , RedisStore = require('connect-redis')(expressSession)
  , app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , cookie = cookieParser(cfg.SECRET)
  , store = new RedisStore({prefix: cfg.KEY})
;

app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');
app.disable('x-powerd-by');
app.use(compression());
app.use(expressSession({
    secret: cfg.SECRET,
    name: cfg.KEY,
    saveUninitialized: true,
    resave: true,
    store: store
}));
app.use(cookie);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('_method'));
app.use('/static', express.static(path.join(__dirname, '/public'), cfg.CACHE));
app.use(csurf());
app.use(function(req, res, next){
  res.locals._csrf = req.csrfToken();
  next();
});

// I NEED STUDY ABOUT IT....
io.use(function(socket, next){
  var data = socket.request;
  cookie(data, {}, function(err){
    var sessionID = data.signedCookies[cfg.KEY];
    store.get(sessionID, function(err, session){
      if(err || !session){
        return next(new Error('access denied'));
      }else{
        socket.handshake.session = session;
        return next();
      }
    });
  });
});

io.adapter(redisAdapter(cfg.REDIS));

// I NEED STUDY ABOUT IT
load('models')
  .then('controllers')
  .then('routes')
  .into(app);
load('sockets')
  .into(io);

server.listen(cfg.SERVER.port, function(){
  console.log("Perfect Tense is working .... Port: %d", cfg.SERVER.port);
});

module.exports = app;
